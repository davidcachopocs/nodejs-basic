'use strict';

const expect = require("chai").expect,
	calc = require('../app/calculator');

describe('Calculator tests', function() {
	describe('Add values', function() {
	  it('add [1, 2] expect to equal 3', function() {
	  	calc.pushValue(1);
	  	calc.pushValue(2);
	  	expect(calc.add()).to.equal(3);
	  });
	});

	describe('Sub values', function() {
	  it('sub [1, 2] expect to equal -1', function() {
	  	calc.pushValue(1);
	  	calc.pushValue(2);
	    expect(calc.sub(1, 2)).to.equal(-1);
	  });
	});

	describe('Mult values', function() {
		it('mult [1, 2] expect to equal 2', function() {
			calc.pushValue(1);
	  	calc.pushValue(2);
			expect(calc.mult(1, 2)).to.equal(2);
		})
	});

	describe('Div values', function() {
		it('div [1, 2] expect to equal 0.5', function() {
			calc.pushValue(1);
	  	calc.pushValue(2);
			expect(calc.div(1, 2)).to.equal(0.5);
		})
	})
});