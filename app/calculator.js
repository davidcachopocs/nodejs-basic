'use strict';

const _ = require('lodash'),
	logger = require('../config/logger').appLogger;

let values = [];

exports.pushValue = (value) => {
	logger.debug(`Push ${value}`);
	values.push(value);
}

exports.add = () => {
	logger.debug(`Add ${values} values`);
	let result = null;
	_.forIn(values, (value, key) => {
		result = !result ? value : result + value;
	})
	values = [];
	return result;
};

exports.sub = () => {
	logger.debug(`Sub ${values} values`);
	let result = null;
	_.forIn(values, (value, key) => {
		result = !result ? value : result - value;
	})
	values = [];
	return result;
};

exports.mult = () => {
	logger.debug(`Mult ${values} values`);
	let result = null;
	_.forIn(values, (value, key) => {
		result = !result ? value : result * value;
	})
	values = [];
	return result;
};

exports.div = () => {
	logger.debug(`Div ${values} values`);
	let result = null;
	_.forIn(values, (value, key) => {
		result = !result ? value : result / value;
	})
	values = [];
	return result;
};