'use strict';

require('./env/env');

const readline = require('readline'),
	logger = require('./config/logger').appLogger,
  events = require('./config/events'),
	calc = require('./app/calculator');

const stdLine = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

logger.info('Calculator ready!');

console.log('Type "quit" to exit. Type a number to store it or a math symbol [+-*/] to calculate the operation of the stored numbers\r\n');
stdLine.prompt();

stdLine.on('line', (value) => {
  logger.debug(`Line ${value} received`);
  if(!isNaN(value)) {
  	calc.pushValue(parseInt(value, 10));	
  } else {
  	let result = null;

  	switch(value) {
  		case '+':
  			result = calc.add();
  			break;
  		case '-':
  			result = calc.sub();
  			break;
  		case '*':
  			result = calc.mult();
  			break;
  		case '/':
  			result = calc.div();
  			break;
  		case 'quit':
  			stdLine.close();
        events.emit('QUIT_APP');
  			break;
  		default:
  			console.log(`Invalid ${value} value\r\n`)
        throw new Error('Invalid value');
  	}

  	if(result) {
  		console.log(`Result: ${result}\r\n`);
  	}    
  }

  stdLine.prompt();
});

events.on('QUIT_APP', () => {
  process.exit(0);
});

process.on('uncaughtException', (error) => {
	logger.error(`UNCAUGHTEXCEPTION --> ${error}`);
});

process.on('exit', (code) => {
	switch(code) {
		/*case -1:
			logger.error(`App stopped because of xxxx`);
			break;*/
		default:
			if(code < 0) {
				logger.error(`Calculator stopped because of unknown error (code: ${code})`);
			} else {
				logger.warn(`Calculator stopped`);
			}
	}
});
